#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
#SingleInstance force


Menu, Tray, Icon, GP5.ico

^!r::Reload  ; Assign Ctrl-Alt-R as a hotkey to restart the script.
GP5HotkeyActive := True


;;;;;;click and move to same position;;;;;;;
CoolClick(posX, posY)
{
	MouseGetPos, CoordXRec, CoordYRec
	MouseMove, %posX%, %posY%
	Click
	MouseMove, %CoordXRec%, %CoordYRec%
	return
}
;;;;;;click and move to same position;;;;;;;

;;;;;;temporary tooltip maker;;;;;;
Tippy(tipsHere, wait:=555)
{
	ToolTip, %tipsHere%
	SetTimer, noTip, %wait% ;--in 1/3 seconds by default, remove the tooltip
}
noTip:
	ToolTip,
	;removes the tooltip
return
;;;;;;temporary tooltip maker;;;;;;

ToggleGP5Hotkeys()
{
global GP5HotkeyActive
    if GP5HotkeyActive 
	{
		GP5HotkeyActive := False
        Menu, Tray, Icon, GP5.ico
		Tippy("Enabled GP5 Pro Hotkeys")
	}
	else
	{
		GP5HotkeyActive := True
        Menu, Tray, Icon, GP5-off.ico
		Tippy("Disabled GP5 Pro Hotkeys")
	}
}

; Return 1 if the the user is targeting a monster
DetectImage(image) {
  ImageSearch, FoundX, FoundY, 0,0, 1280,720, %image%
  if (ErrorLevel=0)
  {
    return 1
  }
  return 0
}

^!g::
    ToggleGP5Hotkeys()
    Return

; detects if guitar pro is active and tooltips are enabled
gp5active(GP5HotkeyActive)
{
	if WinActive("ahk_exe GP5.exe") = 0x0
		return False
	if GP5HotkeyActive
		return False
    if DetectImage("quick-access-star.png")
        return False
    if DetectImage("gp5-cancel.png")
        return False
	return True
}

; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ;
; Guitar Pro 5 Shortcuts
;
#If gp5active(GP5HotkeyActive)
    F1::
        ToggleGP5Hotkeys()
        Return

	q::Send ^+{Ins} 		;Add an instrument
	; m:: ; midi input toggle
		; Send {Alt down}{s}{m}{m}{Alt up}{Enter}
		; Return

	+a::
		Send {Alt down}{e}{e}{Alt up}{Enter}
		Sleep 30
		Send {s}{Right}{s}{s}{Enter}
		Return
	+s::
		Send {Alt down}{e}{e}{Alt up}{Enter}
		Sleep 30
		Send {s}{Right}{s}{s}{s}{Enter}
		Return
	+d::
		Send {Alt down}{e}{e}{Alt up}{Enter}
		Sleep 30
		Send {s}{Right}{s}{s}{s}{s}{Enter}
		Return
	+f::
		Send {Alt down}{e}{e}{Alt up}{Enter}
		Sleep 30
		Send {s}{Right}{s}{s}{s}{s}{s}{Enter}
		Return


; -------------------
; REPLACE WITH IMAGE SEARCHES
; -------------------

	+q::CoolClick(491,68)	;Toggle Multi track
	+w::CoolClick(680,68)	;Open the percussion
	+e::CoolClick(720,68)	;Open the piano
	+t::CoolClick(206,111)	;Triplet
	e::Send {NumpadAdd}		;+
	w::Send {NumpadSub}		;-

	t:: ; Open Transpose window
		Send {Alt down}{t}{t}{Alt up}
		Sleep 500
		Send {Enter}
		Sleep 500
		Return
	d:: ; dots note
		Send {Alt down}{n}{d}{d}{Right}{d}{Alt up}
		Return
    m::
        Send {Alt down}{o}{a}{Alt up}
        Return

	+1::CoolClick(719, 120) ;ppp
	+2::CoolClick(731, 120) ;pp
	+3::CoolClick(764, 120) ;p
	+4::CoolClick(788, 120) ;mp
	+5::CoolClick(807, 120) ;mf
	+6::CoolClick(832, 120) ;f
	+7::CoolClick(857, 120) ;ff
	+8::CoolClick(880, 120) ;fff
#If
